package net.loyin.app.ctrl.front;

import com.jfinal.aop.Before;
import com.jfinal.plugin.ehcache.CacheInterceptor;
import com.jfinal.plugin.ehcache.CacheName;
import net.loyin.ext.annotation.ControllerBind;
import net.loyin.app.model.Article;
import net.loyin.app.model.Leader;
import net.loyin.app.model.NavMenu;
import org.apache.commons.lang.StringUtils;

/**
 * 前端首页
 * Created by loyin on 16/1/12.
 */
@ControllerBind(route =SchoolCtrl.base)
public class SchoolCtrl extends FrontBaseCtrl{
    public static final String base="/school";
    /***
     * 教工之家
     */
    @Before(CacheInterceptor.class)
    @CacheName("page")
    public void jgzj(){
        this.setNavInfo(base+"/jgzj");
        this.keepPara();
        this.setAttr("page",Article.dao.pageQry(this.getParaToInt(0,1),this.getParaToInt(1,8),"jgzj"));
    }
    public void index() {
        String para=this.getPara(0);
        if(StringUtils.isNotBlank(para)) {
            String jumpUrl = NavMenu.dao.hasJumpUrl(base+"/"+ para, 1);
            if (StringUtils.isNotBlank(jumpUrl)) {
                this.forward(jumpUrl+(jumpUrl.indexOf("?")>-1?"&":"?")+"fromUrl="+base+"/"+ para);
            }
        }else {
            this.renderNull();
        }
    }
    /**管理团队*/
    @Before(CacheInterceptor.class)
    @CacheName("page")
    public void gltd(){
        this.setNavInfo(base+"/gltd");
        this.setAttr("leaderList", Leader.dao.find("select * from leader where status=1 order by sort asc"));
    }
}
