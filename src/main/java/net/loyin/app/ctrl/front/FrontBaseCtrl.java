package net.loyin.app.ctrl.front;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import net.loyin.app.icp.AdminInterceptor;
import net.loyin.app.icp.FrontInterceptor;
import net.loyin.app.model.Article;
import net.loyin.app.model.NavMenu;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import java.util.List;

/**
 * 前端基础ctrl类
 * Created by loyin on 16/1/12.
 */
@Clear(AdminInterceptor.class)
@Before(FrontInterceptor.class)
public abstract class FrontBaseCtrl extends Controller {
    public Logger logger= Logger.getLogger(this.getClass());

    protected void setNavInfo(){
        String url=this.getRequest().getRequestURI();
        String fromUrl=this.getPara("fromUrl");
        if(StringUtils.isNotBlank(fromUrl)){
            url=fromUrl;
        }
        this.setNavInfo(url);
    }
    protected void forward(String url){
        String[] urls=url.split("\\?");
        this.forwardAction(urls[0]);
//        this.redirect(url,true);
    }
    protected void setNavInfo(String url){
        this.setAttr("url",url);
        //查询当前菜单
        NavMenu selfNav= NavMenu.dao.findByUrl(url);
        this.setAttr("selfNav",selfNav);
        if(selfNav==null){
            return;
        }
        try {
            //查询父级菜单
            NavMenu parentNav = NavMenu.dao.findById(selfNav.getParentid());

            this.setAttr("parentNav", (parentNav!=null)?parentNav:selfNav);
            //查询同级菜单
            List<NavMenu> tjiNavList = NavMenu.dao.findByParentId((selfNav.getLevelno()==1)?selfNav.getId():selfNav.getParentid());
            this.setAttr("tjiNavList", tjiNavList);
        }catch (Exception e){
            logger.error("",e);
        }
    }

}
