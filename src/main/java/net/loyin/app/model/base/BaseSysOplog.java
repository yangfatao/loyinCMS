package net.loyin.app.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

import java.sql.Timestamp;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseSysOplog<M extends BaseSysOplog<M>> extends Model<M> implements IBean {

	public void setId(String id) {
		set("id", id);
	}

	public String getId() {
		return get("id");
	}

	public void setMenuName(String menuName) {
		set("menu_name", menuName);
	}

	public String getMenuName() {
		return get("menu_name");
	}

	public void setOpresult(String opresult) {
		set("opresult", opresult);
	}

	public String getOpresult() {
		return get("opresult");
	}

	public void setReqData(String reqData) {
		set("req_data", reqData);
	}

	public String getReqData() {
		return get("req_data");
	}

	public void setUrl(String url) {
		set("url", url);
	}

	public String getUrl() {
		return get("url");
	}

	public void setUserId(String userId) {
		set("user_id", userId);
	}

	public String getUserId() {
		return get("user_id");
	}

	public void setIp(String ip) {
		set("ip", ip);
	}

	public String getIp() {
		return get("ip");
	}

	public void setCreateDatetime(java.util.Date createDatetime) {
		set("create_datetime", new Timestamp(createDatetime.getTime()));
	}

	public java.util.Date getCreateDatetime() {
		return getTimestamp("create_datetime");
	}

}
