package net.loyin.app.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseRecruit<M extends BaseRecruit<M>> extends Model<M> implements IBean {

	public void setId(String id) {
		set("id", id);
	}

	public String getId() {
		return get("id");
	}

	public void setDpt(String dpt) {
		set("dpt", dpt);
	}

	public String getDpt() {
		return get("dpt");
	}

	public void setJobname(String jobname) {
		set("jobname", jobname);
	}

	public String getJobname() {
		return get("jobname");
	}

	public void setAmount(Integer amount) {
		set("amount", amount);
	}

	public Integer getAmount() {
		return get("amount");
	}

	public void setEndDate(java.util.Date endDate) {
		set("end_date", endDate);
	}

	public java.util.Date getEndDate() {
		return get("end_date");
	}

	public void setWorkAddr(String workAddr) {
		set("work_addr", workAddr);
	}

	public String getWorkAddr() {
		return get("work_addr");
	}

	public void setWelfare(String welfare) {
		set("welfare", welfare);
	}

	public String getWelfare() {
		return get("welfare");
	}

	public void setQualifications(String qualifications) {
		set("qualifications", qualifications);
	}

	public String getQualifications() {
		return get("qualifications");
	}

	public void setDuty(String duty) {
		set("duty", duty);
	}

	public String getDuty() {
		return get("duty");
	}

	public void setStatus(Integer status) {
		set("status", status);
	}

	public Integer getStatus() {
		return get("status");
	}

	public void setCreateDatetime(java.util.Date createDatetime) {
		set("create_datetime", createDatetime);
	}

	public java.util.Date getCreateDatetime() {
		return get("create_datetime");
	}

}
