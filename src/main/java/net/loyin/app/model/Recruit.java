package net.loyin.app.model;

import net.loyin.ext.annotation.TableBind;
import net.loyin.app.model.base.BaseRecruit;
import java.util.List;
/**
 * 招聘信息
 */
@SuppressWarnings("serial")
@TableBind(tableName = Recruit.tableName)
public class Recruit extends BaseRecruit<Recruit> {
	public static final String tableName="recruit";
	public static final Recruit dao = new Recruit();
	public List<Recruit> findAll(){
		return this.find("select * from recruit where status=1 order by create_datetime desc");
	}
}
