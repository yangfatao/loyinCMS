package net.loyin.app.model;

import net.loyin.ext.annotation.TableBind;
import net.loyin.app.model.base.BaseSysMenu;

import java.util.List;

/**
 * 系统菜单
 */
@SuppressWarnings("serial")
@TableBind(tableName = SysMenu.tableName)
public class SysMenu extends BaseSysMenu<SysMenu> {
	public static final String tableName = "sys_menu";
	public static final SysMenu dao = new SysMenu();

	public List<SysMenu> getAuthMenu(String userid) {
		return this.find("select distinct m.* from sys_menu m,sys_role_menu rm,sys_user_role ur where ur.user_id=? and ur.role_id=rm.role_id and rm.menu_id=m.id and m.status = 1 order by sort asc", userid);
	}
	public List<SysMenu> findAll(){
		return this.find("select * from sys_menu order by parentid asc,sort asc");
	}
}
